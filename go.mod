module ci.tno.nl/gitlab/ppa-project/pkg/paillier

go 1.14

require (
	github.com/rs/zerolog v1.20.0
	github.com/stretchr/testify v1.5.1
)
